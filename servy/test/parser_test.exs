defmodule ParserTest do
  use ExUnit.Case
  doctest Servy.Parser

  alias Servy.Parser
  test "parses a list of header fields into a map" do
    header_lines = ["A: 1", "B: 2"]

    header = Parser.parse_header(header_lines, %{})

    assert header == %{"A" => "1", "B" => "2"}
  end
end
