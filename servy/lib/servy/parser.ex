defmodule Servy.Parser do
  alias Servy.Conv
  def parse(request) do
		[top, params_string] = String.split(request, "\r\n\r\n")

		[request_line | header_lines] = String.split(top, "\r\n")

		[method, path, _] = String.split(request_line, " ")

		header = parse_header(header_lines, %{})#pass an initial empty map

		params = parse_params(header["Content-Type"], params_string)

		%Conv{
      method: method,
				path: path,
				params: params,
				header: header
		 }
	end

	def parse_header([head | tail], header)do
		#	IO.puts "Head: #{inspect(head)} Tail: #{inspect(tail)}" #print the outcome for easy debugging
		[key ,value] = String.split(head, ": ")
		#IO.puts "Key: #{inspect(key)} Value: #{inspect(value)}"
		#create a map to put the key and value
		header = Map.put(header, key, value)
		#IO.inspect header
		parse_header(tail, header)
	end
	#termination function
	def parse_header([], header) do
		header
	end
	# @doc """
	# 	Parses the given param string of the form 'key1=value1&key2=value2'
	# 	into a map with corresponding keys and values.

	# 	## Examples
	# 			iex> params_string = "name=Baloo&type=Brown"
	# 			iex> Servy.Parser.parse_params("application/x-www-form-urlencoded", params_string)
	# 			%{"name" => "Baloo", "type" => "Brown"}
	# 			iex> Servy.Parse.parse_params("multipart/form-data", params_string)
	# 			%{}
	# """
	def parse_params("application/x-www-form-urlencoded", params_string)do
		params_string |> String.trim |> URI.decode_query
	end
	def parse_params(_, _) do
		%{}
	end
end
