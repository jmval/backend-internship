defmodule Servy.Conv do
  #can't define more than one struct per module
  defstruct method: "",
						path: "",
						params: %{},
						header: %{},
						resp_content_type: "text/html",
						resp_body: "",
						status: nil

  @spec full_status(atom | %{:status => any, optional(any) => any}) :: <<_::64, _::_*8>>
  def full_status (conv) do
    "#{conv.status} #{status_reason(conv.status)}"
  end

  defp status_reason(code) do
		%{
				200=>"OK",
				201=>"Created",
				401=>"Unauthorized",
				403=>"Forbidden",
				404=>"Not Found",
				500=>"Internal Server Error"
		 }[code]
	end
end
