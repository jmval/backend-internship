defmodule Servy.Handler do

	@moduledoc "Handles HTTP request."

	alias Servy.Conv
	alias Servy.BearController
	alias Servy.VideoCam
	#alias Servy.Fetcher
	@pages_path Path.expand("../../pages", __DIR__) #any function below this points can use or read the value

	#import Servy.Plugins #import the module to avoid typing the module name when calling
	#doing this tells what function you only want/need
	import Servy.Plugins, only: [rewrite_path: 1, log: 1, track: 1]
	import Servy.Parser, only: [parse: 1]
	@doc "Transform the request into a response."

	def handle(request) do
		#conv = parse(request)
		#conv = route(conv)
		#format_response(conv)
		#using a pipeline '|>'
		request
		|> parse
		|> rewrite_path
		|> log
		|> route
		|> track
		|> format_response
	end

	def route(%Conv{method: "POST", path: "/pledges"} = conv) do
		Servy.PledgeController.create(conv, conv.params)
	end
	def route(%Conv{method: "GET", path: "/pledges"} = conv) do
		Servy.PledgeController.index(conv)
	end

	def route(%Conv{method: "GET", path: "/sensors"} = conv) do
		sensor_data = Servy.SensorServer.get_sensor_data()

		%{ conv | status: 200, resp_body: inspect sensor_data}
	end
	#use short declarative function instead of if else to handle multiple request
	#wildthings
	def route(%Conv{method: "GET", path: "/wildthings"} = conv) do
		%{ conv | status: 200, resp_body: "Bears, Lions, Tigers" }
	end
	#bears
	def route(%Conv{ method: "GET", path: "/bears"} = conv) do
		BearController.index(conv)

	end
	#api/bears
	def route(%Conv{ method: "GET", path: "/api/bears"} = conv) do
		Servy.API.BearController.index(conv)

	end
	#bears/1
	def route(%Conv{ method: "GET", path: "/bears/" <> id} = conv )do
		params = Map.put(conv.params, "id", id)
		BearController.show(conv, params)
	end

	#name=Baloo&type=Brown
	def route(%Conv{method: "POST", path: "/bears"} = conv) do
		BearController.create(conv, conv.params)
	end
	#about
		#using multiple clause function
		def route(%Conv{ method: "GET", path: "/about"} = conv) do
						@pages_path
						|> Path.join("about.html")
						|> File.read
						|> handle_file(conv)
		end
		#default function if path do not match
		def route(%{path: path} = conv) do
			%{ conv | status: 404, resp_body: "No #{path} here!"}
		end
		#200
		def handle_file({:ok, content}, conv) do
			%{ conv | status: 200, resp_body: content}
		end
		#404
		def handle_file({:error, :enoent}, conv) do
			%{ conv | status: 404, resp_body: "File not found!"}
		end
		#500
		def handle_file({:error, reason}, conv) do
			%{ conv | status: 500, resp_body: "File error: #{reason}"}
		end
		#using case
		# def route(%{ method: "GET", path: "/about"} = conv) do
		# 	file =
		# 				Path.expand("../../pages", __DIR__)
		# 				|> Path.join("about.html")
		# 	case File.read(file) do
		# 		{:ok, content} ->
		# 			%{ conv | status: 200, resp_body: content}
		# 		{:error, :enoent} ->
		# 			%{ conv | status: 404, resp_body: "File not found!"}
		# 		{:error, reason} ->
		# 			%{ conv | status: 500, resp_body: "File error: #{reason}"}
		# 	end
		# end

	def format_response (%Conv{} = conv) do
		"""
		HTTP/1.1 #{Conv.full_status(conv)}\r
		Content-Type: #{conv.resp_content_type}\r
		Content-Length: #{String.length(conv.resp_body)}\r
		\r
		#{conv.resp_body}
		"""
	end
end

#wildthings
# request = """
# GET /wildthings HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*

# """

# response = Servy.Handler.handle(request)

# IO.puts response

#bears
# request = """
# GET /bears HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*

# """

# response = Servy.Handler.handle(request)

# IO.puts response

# #bears 1
# request = """
# GET /bears/1 HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*

# """

# response = Servy.Handler.handle(request)

# IO.puts response

# #bigfoot
# request = """
# GET /bigfoot HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*

# """

# response = Servy.Handler.handle(request)

# IO.puts response

# #about
# request = """
# GET /about HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*

# """

# response = Servy.Handler.handle(request)

# IO.puts response


#post bears
# request = """
# POST /bears HTTP/1.1
# Host: example.com
# User-Agent: ExampleBrowser/1.0
# Accept: */*
# Content-Type: application/x-www-form-urlencoded
# Content-Length: 21

# name=Baloo&type=Brown
# """

# response = Servy.Handler.handle(request)

# IO.puts response
